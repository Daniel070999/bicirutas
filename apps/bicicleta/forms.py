from django import forms	
from apps.modelo.models import Bicicleta, Cliente, Coordinador, Ruta

class FormularioBicicleta(forms.ModelForm):
	class Meta:
		model = Bicicleta
		fields = ["chasis", "nro_aro", "marca", "tipo"] 
class FormularioRutaBicicleta(forms.ModelForm):
	class Meta:
		model = Bicicleta
		fields = ["rut_id"] 

class FormularioCliente(forms.ModelForm):
	class Meta:
		model = Cliente
		fields = ["cedula", "nombres", "apellidos", "direccion", "celular", "genero", "edad", "estatura"] 
class FormularioModificarCliente(forms.ModelForm):
	class Meta:
		model = Cliente
		fields = ["nombres", "apellidos", "direccion", "celular", "edad", "estatura"]  
class FormularioModificarCoordinador(forms.ModelForm):
	class Meta:
		model = Coordinador
		fields = ["nombres", "apellidos", "direccion", "celular"]  
class FormularioCoordinador(forms.ModelForm):
	class Meta:
		model = Coordinador
		fields = ["cedula", "nombres", "apellidos", "direccion", "celular", "genero"] 
class FormularioRuta(forms.ModelForm):
	class Meta:
		model = Ruta
		fields = ["distancia","Niveldificultad", "ProbabilidadClima", "lugar","fecha", "hora_inicio", "hora_final"] 


