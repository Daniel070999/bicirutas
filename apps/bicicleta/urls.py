from django.urls import path

from . import views

urlpatterns = [
    path('', views.principalCliente, name= 'cliente'),
    path('registrar_bicicleta/', views.registrarBicicleta),
    path('modificar_cliente/', views.modificarCliente),
    path('modificar_coordinador/', views.modificarCoordinador),
    path('registrar_ruta/', views.registrarRuta),
    path('ver_bicicleta/', views.verBicicleta),
    path('ver_ruta/', views.verRuta),
    path('mis_rutas/', views.misRutas),
    path('nueva_bicicleta/', views.nuevaBicicleta),
    path('nueva_ruta/', views.nuevaRuta),
    path('eliminar_cliente/', views.eliminarCliente),
     path('eliminar_coordinador/', views.eliminarCoordinador),
    path('eliminar_bicicleta/', views.eliminarBicicleta),
    path('lista_bicicletas/', views.listaBicicletas),
    path('lista_Todasrutas/', views.listaTodasRutas),
    path('lista_rutas/', views.listaRutas),
    path(r'rutaBici/(?P<ruta>d+)/(?P<bicicleta>d+)/$', views.seleccionRuta, name = 'rutaBici'),

    ]