from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import FormularioBicicleta, FormularioCliente, FormularioCoordinador, FormularioRuta, FormularioRutaBicicleta, FormularioModificarCliente, FormularioModificarCoordinador
from apps.modelo.models import Bicicleta, Cliente, Coordinador, Ruta
from django.db.models import Q

def principalCliente(request):
	listaC = Coordinador.objects.all().order_by('apellidos')
	lista = Cliente.objects.all().order_by('apellidos')
	bicicleta = Bicicleta.objects.all()
	queryset = request.GET.get("buscarCliente")
	if (queryset):
		lista = Cliente.objects.filter(
			Q(cedula__icontains = queryset) | #__icontains es como un like en mysql
			Q(nombres__icontains = queryset) |
			Q(apellidos__icontains = queryset)
		).distinct()
	queryset = request.GET.get("buscarCoordinador")
	if (queryset):
		listaC = Coordinador.objects.filter(
			Q(cedula__icontains = queryset) | #__icontains es como un like en mysql
			Q(nombres__icontains = queryset) |
			Q(apellidos__icontains = queryset)
		).distinct()
	context = {
		'lista' : lista,
		'listaC': listaC,
		'bicicleta' : bicicleta,
	}
	return render (request, 'cliente/principal_cliente.html', context)
@login_required
def modificarCliente(request):
	cedu = request.GET['cedula']
	cliente = Cliente.objects.get(cedula = cedu)
	usuario = request.user 
	if usuario.groups.filter(name = 'cliente').exists():
		if request.method == 'POST':
			formulario =FormularioModificarCliente(request.POST)
			if formulario.is_valid():
				datos = formulario.cleaned_data
				cliente.nombres = datos.get('nombres')
				cliente.apellidos = datos.get('apellidos')
				cliente.edad = datos.get('edad')
				cliente.celular = datos.get('celular')
				cliente.direccion = datos.get('direccion')
				cliente.estatura = datos.get('estatura')
				cliente.save() 
				return redirect(principalCliente)
		else:
			formulario = FormularioModificarCliente(instance = cliente)
		context= {
			'cedu' : cedu,
			'cliente' : cliente,
			'formulario' : formulario,
		} 
	else:
		return render (request, 'cliente/prohibido.html')
	return render (request, 'cliente/modificar_cliente.html', context)
def modificarCoordinador(request):
	cedu = request.GET['cedula']
	coordinador = Coordinador.objects.get(cedula = cedu)
	usuario = request.user 
	if usuario.groups.filter(name = 'coordinador').exists():
		if request.method == 'POST':
			formulario =FormularioModificarCoordinador(request.POST)
			if formulario.is_valid():
				datos = formulario.cleaned_data
				coordinador.nombres = datos.get('nombres')
				coordinador.apellidos = datos.get('apellidos')
				coordinador.celular = datos.get('celular')
				coordinador.direccion = datos.get('direccion')
				coordinador.save() 
				return redirect(principalCliente)
		else:
			formulario = FormularioModificarCoordinador(instance = coordinador)
		context= {
			'cedu' : cedu,
			'coordinador' : coordinador,
			'formulario' : formulario,
		} 
		
	else:
		return render (request, 'cliente/prohibido.html')
	return render (request, 'coordinador/modificar_coordinador.html', context)

def registrarBicicleta(request):
	formularioBicicleta = FormularioBicicleta(request.POST)
	formularioCliente = FormularioCliente(request.POST)
	usuario = request.user 
	if usuario.groups.filter(name = 'cliente').exists():
		if request.method == 'POST':
			if formularioBicicleta.is_valid() and formularioCliente.is_valid():
				datosCliente = formularioCliente.cleaned_data #obteniendo los datos del formulario cuenta
				cliente = Cliente() #creo un objeto de la clase cliente
				cliente.cedula = datosCliente.get('cedula')
				cliente.nombres = datosCliente.get('nombres')
				cliente.apellidos = datosCliente.get('apellidos')
				cliente.direccion = datosCliente.get('direccion')
				cliente.celular = datosCliente.get('celular')
				cliente.genero = datosCliente.get('genero')
				cliente.edad = datosCliente.get('edad')
				cliente.estatura = datosCliente.get('estatura')
				cliente.save()
				datos = formularioBicicleta.cleaned_data #obteniendo todos los datos del formulario
				bicicleta = Bicicleta() #creo un objeto de la clase cliente
				bicicleta.chasis = datos.get('chasis')
				bicicleta.nro_aro = datos.get('nro_aro')
				bicicleta.marca = datos.get('marca')
				bicicleta.tipo = datos.get('tipo')
				bicicleta.cliente = cliente
				bicicleta.save()
				return redirect(principalCliente)
		
		context = {
		'fB': formularioBicicleta,
		'fC': formularioCliente,	 
		}
	else:
		return render (request, 'cliente/prohibido.html')
	return render (request, 'cliente/registrar_bicicleta.html', context)
def registrarRuta(request):
	formularioRuta = FormularioRuta(request.POST)
	formularioCoordinador = FormularioCoordinador(request.POST)
	usuario = request.user 
	if usuario.groups.filter(name = 'coordinador').exists():
		if request.method == 'POST':
			if formularioCoordinador.is_valid() and formularioRuta.is_valid():
				datosCoordinador = formularioCoordinador.cleaned_data #obteniendo los datos del formulario cuenta
				coordinador = Coordinador() #creo un objeto de la clase cliente
				coordinador.cedula = datosCoordinador.get('cedula')
				coordinador.nombres = datosCoordinador.get('nombres')
				coordinador.apellidos = datosCoordinador.get('apellidos')
				coordinador.direccion = datosCoordinador.get('direccion')
				coordinador.celular = datosCoordinador.get('celular')
				coordinador.genero = datosCoordinador.get('genero')
				coordinador.save()
				datos = formularioRuta.cleaned_data #obteniendo todos los datos del formulario
				ruta = Ruta() #creo un objeto de la clase cliente
				ruta.distancia = datos.get('distancia')
				ruta.Niveldificultad = datos.get('Niveldificultad')
				ruta.ProbabilidadClima = datos.get('ProbabilidadClima')
				ruta.lugar = datos.get('lugar')
				ruta.fecha = datos.get('fecha')
				ruta.hora_inicio = datos.get('hora_inicio')
				ruta.hora_final = datos.get('hora_final')
				ruta.coordinador = coordinador
				ruta.save()
				return redirect(principalCliente)
		
		context = {
		'fC': formularioCoordinador,
		'fR': formularioRuta,	 
		}
	else:
		return render (request, 'cliente/prohibido.html')
	return render (request, 'coordinador/registrar_ruta.html', context)
def verBicicleta(request):
	cliente = request.GET['cedula']
	client = Cliente.objects.get(cedula = cliente)
	bici = Bicicleta.objects.filter(cliente_id = client.cliente_id)
	usuario = request.user 
	if usuario.groups.filter(name = 'cliente').exists():
		context = {
			'client' : client,
			'bici' : bici,
		}
	else:
		return render (request, 'cliente/prohibido.html')
	return render(request, 'bicicleta/ver_bicicleta.html', context)
def verRuta(request):
	coordinador = request.GET['cedula']
	coord = Coordinador.objects.get(cedula = coordinador)
	ruta = Ruta.objects.filter(coordinador_id = coord.coordinador_id)
	usuario = request.user 
	if usuario.groups.filter(name = 'coordinador').exists():
		context = {
			'coord' : coord,
			'ruta' : ruta,
		}
		return render(request, 'coordinador/ver_ruta.html', context)
	else:
		return render (request, 'cliente/prohibido.html')	
def misRutas(request):
	bicicle = request.GET['bicicleta']
	bici = Bicicleta.objects.get(bicicleta_id= bicicle)
	cliente = Cliente.objects.get (cliente_id = bici.cliente_id)
	ruta = Ruta.objects.filter(ruta_id = bici.rut_id)
	if request.method == 'POST':
		formulario = FormularioRutaBicicleta(request.POST)
		if formulario.is_valid():
			bici.rut_id = '0'
			bici.save()
			return redirect(principalCliente)
	else:
		formulario = FormularioRutaBicicleta(instance = bici)
	context = {
		'bici' : bici,
		'ruta' : ruta,
		'cliente' : cliente,
		'formulario': formulario,
	}
	return render(request, 'cliente/mis_rutas.html', context)
def listaRutas(request):
	bicicleta = request.GET['bici']
	bicicle = Bicicleta.objects.get(bicicleta_id = bicicleta)
	ruta = Ruta.objects.all()
	context = {
		'ruta' : ruta,
		'bicicleta' : bicicleta
	}
	return render(request, 'cliente/lista_rutas.html', context)
def seleccionRuta(request,ruta,bicicleta):
	bici = Bicicleta.objects.get(bicicleta_id = bicicleta)
	rut = Ruta.objects.get(ruta_id = ruta)
	coordinador = Coordinador.objects.get(coordinador_id = rut.coordinador_id)
	if request.method == 'POST':
		formulario = FormularioRutaBicicleta(request.POST)
		if bici.rut_id != '0':
			formulario = FormularioRutaBicicleta(instance = bici)
			context = {
			'ruta' : ruta,
			'bicicleta' : bicicleta
			}
			return render(request, 'bicicleta/ruta_ocupada.html', context)	
		else:
			if formulario.is_valid():
				bici.rut_id = ruta
				bici.save()
				return redirect(principalCliente)	
	else:
		formulario = FormularioRutaBicicleta(instance = bici)
	context = {
		'bici' : bici,
		'rut' : rut,
		'coordinador' : coordinador,
		'formulario' : formulario,
	}
	return render(request, 'bicicleta/agregar_ruta.html', context)	

def nuevaBicicleta(request):
	client = request.GET['cedula']
	clien = Cliente.objects.get(cedula = client)
	cli = clien.cliente_id
	formularioBicicleta = FormularioBicicleta(request.POST)
	if request.method == 'POST':
		if  formularioBicicleta.is_valid():
			cliente = Cliente() #creo un objeto de la clase cliente
			datosBici = formularioBicicleta.cleaned_data #obteniendo los datos del formulario cuenta
			bicicleta = Bicicleta() #creo un objeto de la clase cuenta
			bicicleta.chasis = datosBici.get('chasis')
			bicicleta.nro_aro = datosBici.get('nro_aro')
			bicicleta.marca = datosBici.get('marca')
			bicicleta.tipo = datosBici.get('tipo')
			bicicleta.cliente_id = cli
			bicicleta.save()
			return redirect(principalCliente)
	context = {
		'fB': formularioBicicleta, 
	}

	return render(request, 'bicicleta/nueva_bicicleta.html', context)
def nuevaRuta(request):
	coordina = request.GET['cedula']
	coord = Coordinador.objects.get(cedula = coordina)
	coor = coord.coordinador_id
	formularioRuta = FormularioRuta(request.POST)
	if request.method == 'POST':
		if  formularioRuta.is_valid():
			datosRuta = formularioRuta.cleaned_data #obteniendo los datos del formulario cuenta
			ruta = Ruta() #creo un objeto de la clase cuenta
			ruta.distancia = datosRuta.get('distancia')
			ruta.Niveldificultad = datosRuta.get('Niveldificultad')
			ruta.ProbabilidadClima = datosRuta.get('ProbabilidadClima')
			ruta.lugar = datosRuta.get('lugar')
			ruta.fecha = datosRuta.get('fecha')
			ruta.hora_inicio = datosRuta.get('hora_inicio')
			ruta.hora_final = datosRuta.get('hora_final')
			ruta.coordinador_id = coor
			ruta.save()
			return redirect(principalCliente)
	context = {
		'fR': formularioRuta, 
	}

	return render(request, 'coordinador/nueva_ruta.html', context)
def eliminarCliente(request):
	cliente = request.GET['cedula']
	usuario = request.user 
	if usuario.groups.filter(name = 'cliente').exists():
		Cliente.objects.filter(cedula=cliente).delete()
		return redirect(principalCliente)
	else:
		return render (request, 'cliente/prohibido.html')
def eliminarCoordinador(request):
	coordinador = request.GET['cedula']
	usuario = request.user 
	if usuario.groups.filter(name = 'coordinador').exists():
		Coordinador.objects.filter(cedula=coordinador).delete()
		return redirect(principalCliente)
	else:
		return render (request, 'cliente/prohibido.html')
def eliminarBicicleta(request):
	bicicleta = request.GET['bici']
	usuario = request.user 
	if usuario.groups.filter(name = 'cliente').exists():
		Bicicleta.objects.filter(bicicleta_id=bicicleta).delete()
		return redirect(principalCliente)
	else:
		return render (request, 'cliente/prohibido.html')
def listaBicicletas(request):
	bicicletas = Bicicleta.objects.all()
	context = {
		'bicicletas' : bicicletas,
	}
	return render(request, 'bicicleta/lista_bicicleta.html', context)
def listaTodasRutas(request):
	ruta = Ruta.objects.all()
	context = {
		'ruta' : ruta,
	}
	return render(request, 'bicicleta/lista_Todasrutas.html', context)