from django.contrib import admin
from .models import Cliente
from .models import Bicicleta
from .models import Ruta
from .models import Coordinador

class AdminCliente(admin.ModelAdmin):
	list_display = ["cedula", "apellidos", "nombres", "genero","celular"]
	list_editable = ["apellidos", "nombres", "celular"]
	list_filter = ["genero"]
	search_fields = ["cedula", "apellidos"]
	class Meta:
		model = Cliente
admin.site.register(Cliente, AdminCliente)


class AdminBicicleta(admin.ModelAdmin):
	list_display = ["marca", "tipo", "nro_aro"]
	list_editable = ["tipo"]
	list_filter = ["chasis", "tipo"]
	search_fields = ["chasis", "nro_aro"]
	class Meta:
		model = Bicicleta
admin.site.register(Bicicleta, AdminBicicleta)


class AdminRuta(admin.ModelAdmin):
	list_display = ["fecha","distancia","dificultad","ProbabilidadClima"]
	list_editable = ["distancia","ProbabilidadClima"]
	list_filter = ["fecha","ProbabilidadClima"]
	search_fields = ["fecha"]
	class Meta:
		model = Ruta
admin.site.register(Ruta, AdminRuta)

class AdminCoordinador(admin.ModelAdmin):
	list_display = ["nombres","apellidos","cedula","celular"]
	list_editable = ["celular"]
	list_filter = ["apellidos","genero"]
	search_fields = ["apellidos"]
	class Meta:
		model = Coordinador
admin.site.register(Coordinador, AdminCoordinador)