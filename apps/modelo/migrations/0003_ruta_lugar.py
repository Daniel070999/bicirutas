# Generated by Django 2.2.6 on 2020-02-22 18:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modelo', '0002_auto_20191127_2050'),
    ]

    operations = [
        migrations.AddField(
            model_name='ruta',
            name='lugar',
            field=models.CharField(default=1, max_length=100),
            preserve_default=False,
        ),
    ]
