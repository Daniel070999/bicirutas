from django.db import models

class Cliente(models.Model):
	listGenero = (
		('femenino','Femenino'),
		('masculino','Masculino'),
	)

	cliente_id = models.AutoField(primary_key = True)
	cedula = models.CharField(max_length = 10, unique = True, null = False)
	nombres = models.CharField(max_length = 50, null = False)
	apellidos = models.CharField(max_length = 50, null = False)
	direccion = models.TextField(max_length = 100, default = 'sin direccion')
	celular = models.CharField(max_length = 10) 
	genero = models.CharField(max_length = 15, choices = listGenero , default = 'masculino', null = False)
	edad = models.DecimalField(max_digits = 3 , decimal_places =0 , null = False)
	estatura = models.DecimalField(max_digits = 3, decimal_places = 2, null = False)
class Bicicleta(models.Model):
	listTipoBicicleta = (
		('montanera', 'Montanera(MTB)'),
		('ruta', 'Ruta, Urbana(rigida)'),
		('hibrida, ', 'Hibrida(una suspension)'),
		('electrica', 'Electrica'),
		('bmx', 'BMX'),
		('fatBike', 'Fat Bike(llantas gruesas)'),
	)
	bicicleta_id = models.AutoField(primary_key = True)
	chasis = models.CharField(max_length = 25, unique = True, null = False)
	nro_aro = models.DecimalField(max_digits = 3, decimal_places = 1, null = False)
	marca = models.CharField(max_length = 50)
	tipo = models.CharField(max_length = 30, choices = listTipoBicicleta, null = False, default = 'hibrida')
	rut_id = models.CharField(max_length = 11, default = '0')
	cliente = models.ForeignKey(
		'Cliente',
		on_delete = models.CASCADE,
	)
class Ruta(models.Model):
	dificultad = (
		('alto','Alto'),
		('medio','Medio'),
		('bajo','Bajo'),
	)
	proClima = (
		('despejado','Despejado'),
		('nublado','Nublado'),
		('chubasco','Chubasco'),
		('lluvia','Lluvia'),
	)
	ruta_id = models.AutoField(primary_key = True)
	distancia = models.DecimalField(max_digits = 4, decimal_places = 1 , null = False)
	Niveldificultad = models.CharField(max_length = 30, choices = dificultad, null = False)
	ProbabilidadClima = models.CharField(max_length = 30, choices = proClima, null = False)
	lugar = models.CharField(max_length = 100 )
	fecha = models.DateField(auto_now_add = False , null = False)
	hora_inicio = models.TimeField(auto_now_add = False, null = False)
	hora_final = models.TimeField(auto_now_add = False, null = False)
	coordinador = models.ForeignKey(
		'Coordinador',
		on_delete = models.CASCADE,
	)


class Coordinador(models.Model):
	listGenero = (
		('femenino','Femenino'),
		('masculino','Masculino'),
	)
	coordinador_id = models.AutoField(primary_key = True)
	cedula = models.CharField(max_length = 10, unique = True, null = False)
	nombres = models.CharField(max_length = 50, null = False)
	apellidos = models.CharField(max_length = 50, null = False)
	direccion = models.TextField(max_length = 100, default = 'sin direccion')
	celular = models.CharField(max_length = 10) 
	genero = models.CharField(max_length = 15, choices = listGenero , default = 'masculino', null = False)
