from django.shortcuts import render, render_to_response

def PaginaPrincipal(request):
	return render_to_response('pagina_principal.html')